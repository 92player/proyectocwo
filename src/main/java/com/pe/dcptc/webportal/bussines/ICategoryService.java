package com.pe.dcptc.webportal.bussines;

import com.pe.dcptc.webportal.canonic.Category;
import com.pe.dcptc.webportal.canonic.CategoryCreate;
import com.pe.dcptc.webportal.canonic.CategoryDetail;

public interface ICategoryService {
	
	CategoryDetail getCategory (Integer id);
	Category listCategory (Integer pagina, Integer registrosPagina);
	CategoryCreate createCategory (CategoryCreate Category);
	CategoryCreate updateCategory (CategoryCreate category, Integer id);
	void deleteCategory (Integer id);

}
