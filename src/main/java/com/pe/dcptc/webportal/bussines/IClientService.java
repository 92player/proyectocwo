package com.pe.dcptc.webportal.bussines;

import com.pe.dcptc.webportal.canonic.Client;
import com.pe.dcptc.webportal.canonic.ClientCreate;
import com.pe.dcptc.webportal.canonic.ClientDetail;

public interface IClientService {
	
	ClientDetail getClient (Integer id);
	Client listClient (Integer pagina, Integer registrosPagina);
	ClientCreate createClient (ClientCreate client);
	ClientCreate updateClient (ClientCreate client, Integer id);
	void deleteClient (Integer id);

}
