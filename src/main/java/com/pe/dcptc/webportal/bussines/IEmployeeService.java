package com.pe.dcptc.webportal.bussines;

import com.pe.dcptc.webportal.canonic.Employee;
import com.pe.dcptc.webportal.canonic.EmployeeCreate;
import com.pe.dcptc.webportal.canonic.EmployeeDetail;

public interface IEmployeeService {
	
	EmployeeDetail getEmployee (Integer id);
	Employee listEmployee (Integer pagina, Integer registrosPagina);
	EmployeeCreate createEmployee (EmployeeCreate employee);
	EmployeeCreate updateEmployee (EmployeeCreate employee, Integer id);
	void deleteEmployee (Integer id);

}
