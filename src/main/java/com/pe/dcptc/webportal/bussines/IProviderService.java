package com.pe.dcptc.webportal.bussines;

import com.pe.dcptc.webportal.canonic.Provider;
import com.pe.dcptc.webportal.canonic.ProviderCreate;
import com.pe.dcptc.webportal.canonic.ProviderDetail;

public interface IProviderService {
	
	ProviderDetail getProvider (Integer id);
	Provider listProvider (Integer pagina, Integer registrosPagina);
	ProviderCreate createProvider (ProviderCreate provider);
	ProviderCreate updateProvider (ProviderCreate provider, Integer id);
	void deleteProvider (Integer id);

}
