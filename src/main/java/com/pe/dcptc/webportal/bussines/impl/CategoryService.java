package com.pe.dcptc.webportal.bussines.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pe.dcptc.webportal.bussines.ICategoryService;
import com.pe.dcptc.webportal.canonic.Category;
import com.pe.dcptc.webportal.canonic.CategoryCreate;
import com.pe.dcptc.webportal.canonic.CategoryDetail;
import com.pe.dcptc.webportal.dao.ICategoryDAO;

@Component
public class CategoryService implements ICategoryService{
	
	@Autowired
	private ICategoryDAO iCategoryDAO;

	@Override
	public CategoryDetail getCategory(Integer id) {
		return iCategoryDAO.getCategory(id);
	}

	@Override
	public Category listCategory(Integer pagina, Integer registrosPagina) {
		return iCategoryDAO.listCategory(pagina, registrosPagina);
	}

	@Override
	public CategoryCreate createCategory(CategoryCreate category) {
		return iCategoryDAO.createCategory(category);
	}

	@Override
	public CategoryCreate updateCategory(CategoryCreate category, Integer id) {
		return iCategoryDAO.updateCategory(category, id);
	}

	@Override
	public void deleteCategory(Integer id) {
		iCategoryDAO.deleteCategory(id);
		
	}

}
