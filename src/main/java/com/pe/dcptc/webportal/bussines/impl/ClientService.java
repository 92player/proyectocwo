package com.pe.dcptc.webportal.bussines.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pe.dcptc.webportal.bussines.IClientService;
import com.pe.dcptc.webportal.canonic.Client;
import com.pe.dcptc.webportal.canonic.ClientCreate;
import com.pe.dcptc.webportal.canonic.ClientDetail;
import com.pe.dcptc.webportal.dao.IClientDAO;

@Component
public class ClientService implements IClientService{
	
	@Autowired
	private IClientDAO iClientDAO;
	
	@Override
	public ClientDetail getClient(Integer id) {
		return iClientDAO.getClient(id);
	}

	@Override
	public Client listClient(Integer pagina, Integer registrosPagina) {
		return iClientDAO.listClient(pagina, registrosPagina);
	}

	@Override
	public ClientCreate createClient(ClientCreate client) {
		return iClientDAO.createClient(client);
	}

	@Override
	public ClientCreate updateClient(ClientCreate client, Integer id) {
		return iClientDAO.updateClient(client, id);
	}

	@Override
	public void deleteClient(Integer id) {
		iClientDAO.deleteClient(id);
		
	}

}
