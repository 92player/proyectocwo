package com.pe.dcptc.webportal.bussines.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pe.dcptc.webportal.bussines.IEmployeeService;
import com.pe.dcptc.webportal.canonic.Employee;
import com.pe.dcptc.webportal.canonic.EmployeeCreate;
import com.pe.dcptc.webportal.canonic.EmployeeDetail;
import com.pe.dcptc.webportal.dao.IEmployeeDAO;

@Component
public class EmployeeService implements IEmployeeService{
	
	@Autowired
	private IEmployeeDAO iEmployeeDAO;

	@Override
	public EmployeeDetail getEmployee(Integer id) {
		return iEmployeeDAO.getEmployee(id) ;
	}

	@Override
	public Employee listEmployee(Integer pagina, Integer registrosPagina) {
		return iEmployeeDAO.listEmployee(pagina, registrosPagina);
	}

	@Override
	public EmployeeCreate createEmployee(EmployeeCreate employee) {
		return iEmployeeDAO.createEmployee(employee);
	}

	@Override
	public EmployeeCreate updateEmployee(EmployeeCreate employee, Integer id) {
		return iEmployeeDAO.updateEmployee(employee, id);
	}

	@Override
	public void deleteEmployee(Integer id) {
		iEmployeeDAO.deleteEmployee(id);
		
	}

}
