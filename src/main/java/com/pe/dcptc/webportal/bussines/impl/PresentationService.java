package com.pe.dcptc.webportal.bussines.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pe.dcptc.webportal.bussines.IPresentationService;
import com.pe.dcptc.webportal.canonic.Presentation;
import com.pe.dcptc.webportal.canonic.PresentationCreate;
import com.pe.dcptc.webportal.canonic.PresentationDetail;
import com.pe.dcptc.webportal.dao.IPresentationDAO;

@Component
public class PresentationService implements IPresentationService{
	
	@Autowired
	private IPresentationDAO iPresentationDAO;

	@Override
	public PresentationDetail getPresentation(Integer id) {
		return iPresentationDAO.getPresentation(id);
	}

	@Override
	public Presentation listPresentation(Integer pagina, Integer registrosPagina) {
		return iPresentationDAO.listPresentation(pagina, registrosPagina);
	}

	@Override
	public PresentationCreate createPresentation(PresentationCreate presentation) {
		return iPresentationDAO.createPresentation(presentation);
	}

	@Override
	public PresentationCreate updatePresentation(PresentationCreate presentation, Integer id) {
		return iPresentationDAO.updatePresentation(presentation, id);
	}

	@Override
	public void deletePresentation(Integer id) {
		iPresentationDAO.deletePresentation(id);
		
	}

}
