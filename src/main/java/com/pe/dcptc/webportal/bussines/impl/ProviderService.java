package com.pe.dcptc.webportal.bussines.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pe.dcptc.webportal.bussines.IProviderService;
import com.pe.dcptc.webportal.canonic.Provider;
import com.pe.dcptc.webportal.canonic.ProviderCreate;
import com.pe.dcptc.webportal.canonic.ProviderDetail;
import com.pe.dcptc.webportal.dao.IProviderDAO;

@Component
public class ProviderService implements IProviderService{
	
	@Autowired
	private IProviderDAO iProviderDAO;

	@Override
	public ProviderDetail getProvider(Integer id) {
		return iProviderDAO.getProvider(id);
	}

	@Override
	public Provider listProvider(Integer pagina, Integer registrosPagina) {
		return iProviderDAO.listProvider(pagina, registrosPagina);
	}

	@Override
	public ProviderCreate createProvider(ProviderCreate provider) {
		return iProviderDAO.createProvider(provider);
	}

	@Override
	public ProviderCreate updateProvider(ProviderCreate provider, Integer id) {
		return iProviderDAO.updateProvider(provider, id);
	}

	@Override
	public void deleteProvider(Integer id) {
		iProviderDAO.deleteClient(id);
		
	}

}
