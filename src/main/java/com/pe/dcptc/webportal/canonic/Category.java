package com.pe.dcptc.webportal.canonic;

import java.io.Serializable;
import java.util.List;


public class Category implements Serializable {

	private static final long serialVersionUID = -1829455489357607166L;
	
	private List<CategoryList> categoryList;
	private Pagination pagination;
	
	public List<CategoryList> getCategoryList() {
		return categoryList;
	}
	public void setCategoryList(List<CategoryList> categoryList) {
		this.categoryList = categoryList;
	}
	public Pagination getPagination() {
		return pagination;
	}
	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
	}

}
