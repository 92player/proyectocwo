package com.pe.dcptc.webportal.canonic;

import java.io.Serializable;
import java.util.List;

public class Client implements Serializable{

	private static final long serialVersionUID = -1383328969557124387L;
	
	private List<ClientList> clientList;
	private Pagination pagination;
	
	public List<ClientList> getClientList() {
		return clientList;
	}
	public void setClientList(List<ClientList> clientList) {
		this.clientList = clientList;
	}
	public Pagination getPagination() {
		return pagination;
	}
	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
	}

}
