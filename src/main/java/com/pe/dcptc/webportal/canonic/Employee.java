package com.pe.dcptc.webportal.canonic;

import java.io.Serializable;
import java.util.List;

public class Employee implements Serializable{

	private static final long serialVersionUID = -125744668612184712L;
	
	private List<EmployeeList> employeeList;
	private Pagination pagination;
	
	public List<EmployeeList> getEmployeeList() {
		return employeeList;
	}
	public void setEmployeeList(List<EmployeeList> employeeList) {
		this.employeeList = employeeList;
	}
	public Pagination getPagination() {
		return pagination;
	}
	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
	}
	
}
