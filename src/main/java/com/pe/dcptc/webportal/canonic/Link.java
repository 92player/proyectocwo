package com.pe.dcptc.webportal.canonic;

import java.io.Serializable;

public class Link implements Serializable{

	private static final long serialVersionUID = -608008009652659826L;
	
	private String linksFirst;
	private String linksLast;
	private String linksPrevios;
	private String linksNext;
	
	public String getLinksFirst() {
		return linksFirst;
	}
	public void setLinksFirst(String linksFirst) {
		this.linksFirst = linksFirst;
	}
	public String getLinksLast() {
		return linksLast;
	}
	public void setLinksLast(String linksLast) {
		this.linksLast = linksLast;
	}
	public String getLinksPrevios() {
		return linksPrevios;
	}
	public void setLinksPrevios(String linksPrevios) {
		this.linksPrevios = linksPrevios;
	}
	public String getLinksNext() {
		return linksNext;
	}
	public void setLinksNext(String linksNext) {
		this.linksNext = linksNext;
	}

}
