package com.pe.dcptc.webportal.canonic;

import java.io.Serializable;

public class Pagination implements Serializable{

	private static final long serialVersionUID = 7040837161042707054L;
	
	private Link links;
	private Integer page;
	private Integer totalPages;
	private Integer totalElements;
	private Integer pageSize;
	
	public Link getLinks() {
		return links;
	}
	public void setLinks(Link links) {
		this.links = links;
	}
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public Integer getTotalPages() {
		return totalPages;
	}
	public void setTotalPages(Integer totalPages) {
		this.totalPages = totalPages;
	}
	public Integer getTotalElements() {
		return totalElements;
	}
	public void setTotalElements(Integer totalElements) {
		this.totalElements = totalElements;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

}
