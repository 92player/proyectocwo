package com.pe.dcptc.webportal.canonic;

import java.io.Serializable;
import java.util.List;

public class Presentation implements Serializable{

	private static final long serialVersionUID = 8758595150209055224L;
	
	private List<PresentationList> presentationList;
	private Pagination pagination;
	
	public List<PresentationList> getPresentationList() {
		return presentationList;
	}
	public void setPresentationList(List<PresentationList> presentationList) {
		this.presentationList = presentationList;
	}
	public Pagination getPagination() {
		return pagination;
	}
	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
	}

}
