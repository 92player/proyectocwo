package com.pe.dcptc.webportal.canonic;

import java.io.Serializable;

public class PresentationCreate implements Serializable{

	private static final long serialVersionUID = -7430186395507252167L;
	
	private Integer id;
	private String name;
	private String description;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

}
