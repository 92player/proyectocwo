package com.pe.dcptc.webportal.canonic;

import java.io.Serializable;
import java.util.List;

public class Provider implements Serializable{

	private static final long serialVersionUID = 8155847667061744162L;
	
	private List<ProviderList> providerList;
	private Pagination pagination;
	
	public List<ProviderList> getProviderList() {
		return providerList;
	}
	public void setProviderList(List<ProviderList> providerList) {
		this.providerList = providerList;
	}
	public Pagination getPagination() {
		return pagination;
	}
	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
	}	

}
