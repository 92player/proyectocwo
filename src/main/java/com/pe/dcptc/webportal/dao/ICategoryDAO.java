package com.pe.dcptc.webportal.dao;

import com.pe.dcptc.webportal.canonic.Category;
import com.pe.dcptc.webportal.canonic.CategoryCreate;
import com.pe.dcptc.webportal.canonic.CategoryDetail;

public interface ICategoryDAO {
	
	CategoryDetail getCategory (Integer id);
	Category listCategory (Integer pagina, Integer registrosPagina);
	CategoryCreate createCategory (CategoryCreate category);
	CategoryCreate updateCategory (CategoryCreate category, Integer id);
	void deleteCategory (Integer id);

}
