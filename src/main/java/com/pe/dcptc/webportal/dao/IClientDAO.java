package com.pe.dcptc.webportal.dao;

import com.pe.dcptc.webportal.canonic.Client;
import com.pe.dcptc.webportal.canonic.ClientCreate;
import com.pe.dcptc.webportal.canonic.ClientDetail;

public interface IClientDAO {
	
	ClientDetail getClient (Integer id);
	Client listClient (Integer pagina, Integer registrosPagina);
	ClientCreate createClient (ClientCreate client);
	ClientCreate updateClient (ClientCreate client, Integer id);
	void deleteClient (Integer id);

}
