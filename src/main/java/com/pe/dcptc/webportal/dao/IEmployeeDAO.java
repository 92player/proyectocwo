package com.pe.dcptc.webportal.dao;

import com.pe.dcptc.webportal.canonic.Employee;
import com.pe.dcptc.webportal.canonic.EmployeeCreate;
import com.pe.dcptc.webportal.canonic.EmployeeDetail;

public interface IEmployeeDAO {
	
	EmployeeDetail getEmployee (Integer id);
	Employee listEmployee (Integer pagina, Integer registrosPagina);
	EmployeeCreate createEmployee (EmployeeCreate employee);
	EmployeeCreate updateEmployee (EmployeeCreate employee, Integer id);
	void deleteEmployee (Integer id);

}
