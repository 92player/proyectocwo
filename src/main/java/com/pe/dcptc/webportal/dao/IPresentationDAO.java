package com.pe.dcptc.webportal.dao;

import com.pe.dcptc.webportal.canonic.Presentation;
import com.pe.dcptc.webportal.canonic.PresentationCreate;
import com.pe.dcptc.webportal.canonic.PresentationDetail;

public interface IPresentationDAO {
	
	PresentationDetail getPresentation (Integer id);
	Presentation listPresentation (Integer pagina, Integer registrosPagina);
	PresentationCreate createPresentation (PresentationCreate presentation);
	PresentationCreate updatePresentation (PresentationCreate presentation, Integer id);
	void deletePresentation (Integer id);

}
