package com.pe.dcptc.webportal.dao;

import com.pe.dcptc.webportal.canonic.Provider;
import com.pe.dcptc.webportal.canonic.ProviderCreate;
import com.pe.dcptc.webportal.canonic.ProviderDetail;

public interface IProviderDAO {
	
	ProviderDetail getProvider (Integer id);
	Provider listProvider (Integer pagina, Integer registrosPagina);
	ProviderCreate createProvider (ProviderCreate provider);
	ProviderCreate updateProvider (ProviderCreate provider, Integer id);
	void deleteClient (Integer id);

}
