package com.pe.dcptc.webportal.dao.impl;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.pe.dcptc.webportal.canonic.Category;
import com.pe.dcptc.webportal.canonic.CategoryCreate;
import com.pe.dcptc.webportal.canonic.CategoryDetail;
import com.pe.dcptc.webportal.canonic.CategoryList;
import com.pe.dcptc.webportal.canonic.Pagination;
import com.pe.dcptc.webportal.dao.ICategoryDAO;
import com.pe.dcptc.webportal.dao.rowmapper.CategoryDetailRowMapper;
import com.pe.dcptc.webportal.dao.rowmapper.CategoryListlRowMapper;
import com.pe.dcptc.webportal.exceptions.CustomerException;
import com.pe.dcptc.webportal.utilitario.Paginado;

@Component
public class CategoryDAO implements ICategoryDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private static final Logger LOG = Logger.getLogger(CategoryDAO.class);

	Gson gson = new Gson();

	HttpStatus internalServerError = HttpStatus.INTERNAL_SERVER_ERROR;
	HttpStatus notFound = HttpStatus.NOT_FOUND;

	@Transactional
	@Override
	public CategoryDetail getCategory(Integer id) {
		CategoryDetail categoryDetail = new CategoryDetail();
		try {
			LOG.info("...Inicio del metodo getCategory...");
			categoryDetail = (CategoryDetail) jdbcTemplate.queryForObject("SELECT * FROM CATEGORIA WHERE nin_id_categoria = ?", 
					new Object[] { id }, new CategoryDetailRowMapper());
		} catch (Exception e) {
			String descriptionError = String.valueOf(e.getMessage());
			LOG.error(descriptionError);
			throw new CustomerException("001", internalServerError, "Could not found ProgramIncrement with id " + id);
		}
		String JSON = gson.toJson(categoryDetail);
		LOG.info("response: " + JSON);
		LOG.info("...Fin del metodo getCategory...");

		return categoryDetail;
	}

	@Transactional
	@Override
	public Category listCategory(Integer pagina, Integer registrosPagina) {
		Category category = new Category();
		List<CategoryList> categoryList = new ArrayList<>();
		try {
			LOG.info("...Inicio del metodo listCategory...");
			categoryList = (List<CategoryList>) jdbcTemplate.query(
					"select * from CATEGORIA" + Paginado.tramaPaginacion(pagina, registrosPagina),
					new CategoryListlRowMapper());

			Integer totalElementos = jdbcTemplate.queryForObject("SELECT count(*) from CATEGORIA", Integer.class);

			Pagination pagination = new Pagination();
			pagination.setPage(pagina);
			pagination.setPageSize(registrosPagina);
			pagination.setTotalPages((totalElementos / registrosPagina) + 1);
			pagination.setTotalElements(totalElementos);

			category.setCategoryList(categoryList);
			category.setPagination(pagination);

		} catch (Exception e) {
			String descripcionError = String.valueOf(e.getMessage());
			LOG.error(descripcionError);
			throw new CustomerException("0001", internalServerError, "Internal error in database");
		}
		String JSON = gson.toJson(categoryList);
		LOG.info("response: " + JSON);
		LOG.info("...Fin del metodo listCategory...");
		return category;
	}
	
	@Override
	public CategoryCreate createCategory(CategoryCreate category) {
		try {
			LOG.info("...Inicio del metodo createCategory...");
			KeyHolder keyHolder = new GeneratedKeyHolder();
			String query = "INSERT INTO CATEGORIA (nin_id_categoria, txt_nombre_categoria, txt_descripcion_categoria) VALUES (?,?,?)";
			jdbcTemplate.update(connection -> {
				PreparedStatement ps = connection.prepareStatement(query);
				ps.setInt(1, category.getId());
				ps.setString(2, category.getName());
				ps.setString(3, category.getDescription());
				return ps;
			}, keyHolder);
		} catch (Exception e) {
			String descripcionError = String.valueOf(e.getMessage());
			LOG.error(descripcionError);
			throw new CustomerException("0001", notFound, "There is a null parameter");
		}
		String JSON = gson.toJson(category);
		LOG.info("response:" + JSON);
		LOG.info("...Fin del metodo createCategory...");
		return category;
	}
	

	@Override
	public CategoryCreate updateCategory(CategoryCreate category, Integer id) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		try {
			LOG.info("...Inicio del metodo updateCategory...");
			String query = "UPDATE CATEGORIA SET txt_nombre_categoria=?, txt_descripcion_categoria=? WHERE nin_id_categoria=?";
			jdbcTemplate.update(connection -> {
				PreparedStatement ps = connection.prepareStatement(query);
				ps.setString(1, category.getName());
				ps.setString(2, category.getDescription());
				ps.setInt(3, category.getId());
				return ps;		
			}, keyHolder);
		}catch (Exception e) {
			String descripcionError = String.valueOf(e.getMessage());
			LOG.error(descripcionError);
			throw new CustomerException("0001", notFound, "There is a null parameter");
		}
		String JSON = gson.toJson(category);
		LOG.info("response:" + JSON);
		LOG.info("...Fin del metodo updateCategory...");
		return category;

	}

	@Override
	public void deleteCategory(Integer id) {
		try {
			LOG.info("...Inicio del metodo deleteCategory...");
			jdbcTemplate.update("DELETE FROM CATEGORIA WHERE nin_id_categoria = ?", new Object[] { id });
		} catch (Exception e) {
			String descripcionError = String.valueOf(e.getMessage());
			LOG.error(descripcionError);
			throw new CustomerException("0001", internalServerError, "Could not found WorkOrder with id " + id);
		}
		LOG.info("...Fin del metodo deleteCategory...");
	}

}
