package com.pe.dcptc.webportal.dao.impl;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.pe.dcptc.webportal.canonic.Client;
import com.pe.dcptc.webportal.canonic.ClientCreate;
import com.pe.dcptc.webportal.canonic.ClientDetail;
import com.pe.dcptc.webportal.canonic.ClientList;
import com.pe.dcptc.webportal.canonic.Pagination;
import com.pe.dcptc.webportal.dao.IClientDAO;
import com.pe.dcptc.webportal.dao.rowmapper.ClientDetailRowMapper;
import com.pe.dcptc.webportal.dao.rowmapper.ClientListRowMapper;
import com.pe.dcptc.webportal.exceptions.CustomerException;
import com.pe.dcptc.webportal.utilitario.Paginado;

@Component
public class ClientDAO implements IClientDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private static final Logger LOG = Logger.getLogger(ClientDAO.class);

	Gson gson = new Gson();

	HttpStatus internalServerError = HttpStatus.INTERNAL_SERVER_ERROR;
	HttpStatus notFound = HttpStatus.NOT_FOUND;

	@Transactional
	@Override
	public ClientDetail getClient(Integer id) {
		ClientDetail clientDetail = new ClientDetail();
		try {
			LOG.info("...Inicio del metodo getclient...");
			clientDetail = (ClientDetail) jdbcTemplate.queryForObject("SELECT * FROM CLIENTE WHERE nin_id_cliente = ?",
					new Object[] { id }, new ClientDetailRowMapper());
		} catch (Exception e) {
			String descriptionError = String.valueOf(e.getMessage());
			LOG.error(descriptionError);
			throw new CustomerException("001", internalServerError, "Could not found ProgramIncrement with id " + id);
		}
		String JSON = gson.toJson(clientDetail);
		LOG.info("response: " + JSON);
		LOG.info("...Fin del metodo getclient...");

		return clientDetail;
	}

	@Transactional
	@Override
	public Client listClient(Integer pagina, Integer registrosPagina) {
		Client client = new Client();
		List<ClientList> clientList = new ArrayList<>();
		try {
			LOG.info("...Inicio del metodo listClient...");
			clientList = (List<ClientList>) jdbcTemplate.query(
					"select * from CLIENTE" + Paginado.tramaPaginacion(pagina, registrosPagina),
					new ClientListRowMapper());

			Integer totalElementos = jdbcTemplate.queryForObject("SELECT count(*) from CLIENTE", Integer.class);

			Pagination pagination = new Pagination();
			pagination.setPage(pagina);
			pagination.setPageSize(registrosPagina);
			pagination.setTotalPages((totalElementos / registrosPagina) + 1);
			pagination.setTotalElements(totalElementos);

			client.setClientList(clientList);
			client.setPagination(pagination);

		} catch (Exception e) {
			String descripcionError = String.valueOf(e.getMessage());
			LOG.error(descripcionError);
			throw new CustomerException("0001", internalServerError, "Internal error in database");
		}
		String JSON = gson.toJson(clientList);
		LOG.info("response: " + JSON);
		LOG.info("...Fin del metodo listClient...");
		return client;
	}

	@Override
	public ClientCreate createClient(ClientCreate client) {
		try {
			LOG.info("...Inicio del metodo createClient...");
			KeyHolder keyHolder = new GeneratedKeyHolder();
			String query = "INSERT INTO CLIENTE (nin_id_cliente, txt_nombre_cliente, txt_apellido_cliente, txt_sexo_cliente, fec_nacimiento_cliente, txt_documento_cliente, num_numero_documento_cliente, txt_address_cliente, txt_ciudad_cliente, num_telefono_cliente, txt_email_cliente) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
			jdbcTemplate.update(connection -> {
				PreparedStatement ps = connection.prepareStatement(query);
				ps.setInt(1, client.getId());
				ps.setString(2, client.getName());
				ps.setString(3, client.getLastName());
				ps.setString(4, client.getSexo());
				ps.setDate(5, client.getDateBirth());
				ps.setString(6, client.getdocument());
				ps.setInt(7, client.getDocumentNumber());
				ps.setString(8, client.getAddress());
				ps.setString(9, client.getCity());
				ps.setInt(10, client.getPhone());
				ps.setString(11, client.getEmail());
				return ps;
			}, keyHolder);
		} catch (Exception e) {
			String descripcionError = String.valueOf(e.getMessage());
			LOG.error(descripcionError);
			throw new CustomerException("0001", notFound, "There is a null parameter");
		}
		String JSON = gson.toJson(client);
		LOG.info("response:" + JSON);
		LOG.info("...Fin del metodo createClient...");
		return client;
	}

	@Override
	public ClientCreate updateClient(ClientCreate client, Integer id) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		try {
			LOG.info("...Inicio del metodo updateClient...");
			String query = "UPDATE CLIENTE SET txt_nombre_cliente=?, txt_apellido_cliente=?, txt_sexo_cliente=?, fec_nacimiento_cliente=?, txt_documento_cliente=?, num_numero_documento_cliente=?, txt_address_cliente=?, txt_ciudad_cliente=?, num_telefono_cliente=?, txt_email_cliente=? WHERE nin_id_cliente=?";
			jdbcTemplate.update(connection -> {
				PreparedStatement ps = connection.prepareStatement(query);
				ps.setString(1, client.getName());
				ps.setString(2, client.getLastName());
				ps.setString(3, client.getSexo());
				ps.setDate(4, client.getDateBirth());
				ps.setString(5, client.getdocument());
				ps.setInt(6, client.getDocumentNumber());
				ps.setString(7, client.getAddress());
				ps.setString(8, client.getCity());
				ps.setInt(9, client.getPhone());
				ps.setString(10, client.getEmail());
				ps.setInt(11, id);
				return ps;
			}, keyHolder);
		} catch (Exception e) {
			String descripcionError = String.valueOf(e.getMessage());
			LOG.error(descripcionError);
			throw new CustomerException("0001", notFound, "There is a null parameter");
		}
		String JSON = gson.toJson(client);
		LOG.info("response:" + JSON);
		LOG.info("...Fin del metodo updateClient...");
		return client;
	}

	@Override
	public void deleteClient(Integer id) {
		try {
			LOG.info("...Inicio del metodo deleteClient...");
			jdbcTemplate.update("DELETE FROM CLIENTE WHERE nin_id_cliente = ?", new Object[] { id });

		} catch (Exception e) {
			String descripcionError = String.valueOf(e.getMessage());
			LOG.error(descripcionError);
			throw new CustomerException("0001", internalServerError, "Could not found WorkOrder with id " + id);
		}
		LOG.info("...Fin del metodo deleteClient...");
	}

}
