package com.pe.dcptc.webportal.dao.impl;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.pe.dcptc.webportal.canonic.Employee;
import com.pe.dcptc.webportal.canonic.EmployeeCreate;
import com.pe.dcptc.webportal.canonic.EmployeeDetail;
import com.pe.dcptc.webportal.canonic.EmployeeList;
import com.pe.dcptc.webportal.canonic.Pagination;
import com.pe.dcptc.webportal.dao.IEmployeeDAO;
import com.pe.dcptc.webportal.dao.rowmapper.EmployeeDetailRowMapper;
import com.pe.dcptc.webportal.dao.rowmapper.EmployeeListRowMapper;
import com.pe.dcptc.webportal.exceptions.CustomerException;
import com.pe.dcptc.webportal.utilitario.Paginado;

@Component
public class EmployeeDAO implements IEmployeeDAO{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	private static final Logger LOG = Logger.getLogger(ClientDAO.class);

	Gson gson = new Gson();
	
	HttpStatus internalServerError = HttpStatus.INTERNAL_SERVER_ERROR;
	HttpStatus notFound = HttpStatus.NOT_FOUND;
	
	@Transactional
	@Override
	public EmployeeDetail getEmployee(Integer id) {
		EmployeeDetail employeeDetail = new EmployeeDetail();
		try {
			LOG.info("...Inicio del metodo getEmployee...");
			employeeDetail = (EmployeeDetail) jdbcTemplate.queryForObject("SELECT * FROM EMPLEADO WHERE nin_id_empleado = ?",
					new Object[] { id }, new EmployeeDetailRowMapper());			
		}catch (Exception e) {
			String descriptionError = String.valueOf(e.getMessage());
			LOG.error(descriptionError);
			throw new CustomerException("001", internalServerError, "Could not found ProgramIncrement with id " + id);
		}
		String JSON = gson.toJson(employeeDetail);
		LOG.info("response: " + JSON);
		LOG.info("...Fin del metodo getEmployee...");
		
		return employeeDetail;
	}
	
	@Transactional
	@Override
	public Employee listEmployee(Integer pagina, Integer registrosPagina) {
		Employee employee = new Employee();
		List<EmployeeList> employeeList = new ArrayList<>();
		try {
			LOG.info("...Inicio del metodo listEmployee...");
			employeeList = (List<EmployeeList>) jdbcTemplate.query(
					"select * from EMPLEADO" + Paginado.tramaPaginacion(pagina, registrosPagina),
					new EmployeeListRowMapper());
			
			Integer totalElementos = jdbcTemplate.queryForObject("SELECT count(*) from EMPLEADO", Integer.class);
			
			Pagination pagination = new Pagination();
			pagination.setPage(pagina);
			pagination.setPageSize(registrosPagina);
			pagination.setTotalPages((totalElementos / registrosPagina) + 1);
			pagination.setTotalElements(totalElementos);

			employee.setEmployeeList(employeeList);
			employee.setPagination(pagination);
			
		} catch (Exception e) {
			String descripcionError = String.valueOf(e.getMessage());
			LOG.error(descripcionError);
			throw new CustomerException("0001", internalServerError, "Internal error in database");
		}
		String JSON = gson.toJson(employeeList);
		LOG.info("response: " + JSON);
		LOG.info("...Fin del metodo listEmployee...");
		return employee;
	}
		
	@Override
	public EmployeeCreate createEmployee(EmployeeCreate employee) {
		try {
			LOG.info("...Inicio del metodo createEmployee...");
			KeyHolder keyHolder = new GeneratedKeyHolder();
			String query = "INSERT INTO EMPLEADO (nin_id_empleado, txt_nombre_empleado, txt_apellido_empleado, txt_sexo_empleado, fec_nacimiento_empleado, num_numero_documento_empleado, txt_address_empleado, num_telefono_empleado, txt_email_empleado, txt_acceso_empleado, txt_usuario_empleado, txt_contrasena_empleado) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
			jdbcTemplate.update(connection -> {
				PreparedStatement ps = connection.prepareStatement(query);
				ps.setInt(1, employee.getId());
				ps.setString(2, employee.getName());
				ps.setString(3, employee.getLastName());
				ps.setString(4, employee.getSexo());
				ps.setDate(5, employee.getDateBirth());
				ps.setInt(6, employee.getDocumentNumber());
				ps.setString(7, employee.getAddress());
				ps.setInt(8, employee.getPhone());
				ps.setString(9, employee.getEmail());
				ps.setString(10, employee.getAcces());
				ps.setString(11, employee.getUser());
				ps.setString(12, employee.getPassword());	
				return ps;
			}, keyHolder);
		} catch (Exception e) {
			String descripcionError = String.valueOf(e.getMessage());
			LOG.error(descripcionError);
			throw new CustomerException("0001", notFound, "There is a null parameter");
		}
		String JSON = gson.toJson(employee);
		LOG.info("response:" + JSON);
		LOG.info("...Fin del metodo createEmployee...");
		return employee;
	}
	
	@Override
	public EmployeeCreate updateEmployee(EmployeeCreate employee, Integer id) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		try {
			LOG.info("...Inicio del metodo updateEmployee...");
			String query = "UPDATE EMPLEADO SET txt_nombre_empleado=?, txt_apellido_empleado=?, txt_sexo_empleado=?, fec_nacimiento_empleado=?, num_numero_documento_empleado=?, txt_address_empleado=?, num_telefono_empleado=?, txt_email_empleado=?, txt_acceso_empleado=?, txt_usuario_empleado=?, txt_contrasena_empleado=? WHERE nin_id_empleado=?";
			jdbcTemplate.update(connection -> {
				PreparedStatement ps = connection.prepareStatement(query);
				ps.setString(1, employee.getName());
				ps.setString(2, employee.getLastName());
				ps.setString(3, employee.getSexo());
				ps.setDate(4, employee.getDateBirth());
				ps.setInt(5, employee.getDocumentNumber());
				ps.setString(6, employee.getAddress());
				ps.setInt(7, employee.getPhone());
				ps.setString(8, employee.getEmail());
				ps.setString(9, employee.getAcces());
				ps.setString(10, employee.getUser());
				ps.setString(11, employee.getPassword());	
				ps.setInt(12, id);
				return ps;
			}, keyHolder);
		} catch (Exception e) {
			String descripcionError = String.valueOf(e.getMessage());
			LOG.error(descripcionError);
			throw new CustomerException("0001", notFound, "There is a null parameter");
		}
		String JSON = gson.toJson(employee);
		LOG.info("response:" + JSON);
		LOG.info("...Fin del metodo updateEmployee...");
		return employee;
	}
	
	@Override
	public void deleteEmployee(Integer id) {
		try {
			LOG.info("...Inicio del metodo deleteEmployee...");
			jdbcTemplate.update("DELETE FROM EMPLEADO WHERE nin_id_empleado = ?", new Object[] { id });

		} catch (Exception e) {
			String descripcionError = String.valueOf(e.getMessage());
			LOG.error(descripcionError);
			throw new CustomerException("0001", internalServerError, "Could not found WorkOrder with id " + id);
		}
		LOG.info("...Fin del metodo deleteEmployee...");
		
	}
	
}
