package com.pe.dcptc.webportal.dao.impl;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.pe.dcptc.webportal.canonic.Pagination;
import com.pe.dcptc.webportal.canonic.Presentation;
import com.pe.dcptc.webportal.canonic.PresentationCreate;
import com.pe.dcptc.webportal.canonic.PresentationDetail;
import com.pe.dcptc.webportal.canonic.PresentationList;
import com.pe.dcptc.webportal.dao.IPresentationDAO;
import com.pe.dcptc.webportal.dao.rowmapper.PresentationDetailRowwMapper;
import com.pe.dcptc.webportal.dao.rowmapper.PresentationListRowwMapper;
import com.pe.dcptc.webportal.exceptions.CustomerException;
import com.pe.dcptc.webportal.utilitario.Paginado;

@Component
public class PresentationDAO implements IPresentationDAO{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private static final Logger LOG = Logger.getLogger(PresentationDAO.class);
	
	Gson gson = new Gson();

	HttpStatus internalServerError = HttpStatus.INTERNAL_SERVER_ERROR;
	HttpStatus notFound = HttpStatus.NOT_FOUND;
	
	@Transactional
	@Override
	public PresentationDetail getPresentation(Integer id) {
		PresentationDetail presentationDetail = new PresentationDetail();
		try {
			LOG.info("...Inicio del metodo getPresentation...");
			presentationDetail = (PresentationDetail) jdbcTemplate.queryForObject("SELECT * FROM PRESENTACION WHERE nin_id_presentacion = ?", 
					new Object[] { id }, new PresentationDetailRowwMapper());
		} catch (Exception e) {
			String descriptionError = String.valueOf(e.getMessage());
			LOG.error(descriptionError);
			throw new CustomerException("001", internalServerError, "Could not found ProgramIncrement with id " + id);
		}
		String JSON = gson.toJson(presentationDetail);
		LOG.info("response: " + JSON);
		LOG.info("...Fin del metodo getPresentation...");
		
		return presentationDetail;
	}
	
	@Transactional
	@Override
	public Presentation listPresentation(Integer pagina, Integer registrosPagina) {
		Presentation presentation = new Presentation();
		List<PresentationList> presentationList = new ArrayList<>();
		try {
			LOG.info("...Inicio del metodo listPresentation...");
			presentationList = (List<PresentationList>) jdbcTemplate.query(
					"select * from PRESENTACION" + Paginado.tramaPaginacion(pagina, registrosPagina),
					new PresentationListRowwMapper());

			Integer totalElementos = jdbcTemplate.queryForObject("SELECT count(*) from PRESENTACION", Integer.class);

			Pagination pagination = new Pagination();
			pagination.setPage(pagina);
			pagination.setPageSize(registrosPagina);
			pagination.setTotalPages((totalElementos / registrosPagina) + 1);
			pagination.setTotalElements(totalElementos);

			presentation.setPresentationList(presentationList);
			presentation.setPagination(pagination);
			
		} catch (Exception e) {
			String descripcionError = String.valueOf(e.getMessage());
			LOG.error(descripcionError);
			throw new CustomerException("0001", internalServerError, "Internal error in database");
		}		
		String JSON = gson.toJson(presentationList);
		LOG.info("response: " + JSON);
		LOG.info("...Fin del metodo listPresentation...");
		return presentation;
	}

	@Override
	public PresentationCreate createPresentation(PresentationCreate presentation) {
		try {
			LOG.info("...Inicio del metodo createPresentation...");
			KeyHolder keyHolder = new GeneratedKeyHolder();
			String query = "INSERT INTO PRESENTACION (nin_id_presentacion, txt_nombre_presentacion, txt_descripcion_presentacion) VALUES (?,?,?)";
			jdbcTemplate.update(connection -> {
				PreparedStatement ps = connection.prepareStatement(query);
				ps.setInt(1, presentation.getId());
				ps.setString(2, presentation.getName());
				ps.setString(3, presentation.getDescription());
				return ps;
			}, keyHolder);
		} catch (Exception e) {
			String descripcionError = String.valueOf(e.getMessage());
			LOG.error(descripcionError);
			throw new CustomerException("0001", notFound, "There is a null parameter");
		}
		String JSON = gson.toJson(presentation);
		LOG.info("response:" + JSON);
		LOG.info("...Fin del metodo createPresentation...");		
		return presentation;
	}
	

	@Override
	public PresentationCreate updatePresentation(PresentationCreate presentation, Integer id) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		try {
			LOG.info("...Inicio del metodo updatePresentation...");
			String query = "UPDATE PRESENTACION SET txt_nombre_presentacion=?, txt_descripcion_presentacion=? WHERE nin_id_presentacion=?";
			jdbcTemplate.update(connection -> {
				PreparedStatement ps = connection.prepareStatement(query);
				ps.setString(1, presentation.getName());
				ps.setString(2, presentation.getDescription());
				ps.setInt(3, presentation.getId());
				return ps;		
			}, keyHolder);
		}catch (Exception e) {
			String descripcionError = String.valueOf(e.getMessage());
			LOG.error(descripcionError);
			throw new CustomerException("0001", notFound, "There is a null parameter");
		}
		String JSON = gson.toJson(presentation);
		LOG.info("response:" + JSON);
		LOG.info("...Fin del metodo updatePresentation...");
		return presentation;
		
	}

	@Override
	public void deletePresentation(Integer id) {
		try {
			LOG.info("...Inicio del metodo deletePresentation...");
			jdbcTemplate.update("DELETE FROM PRESENTACION WHERE nin_id_presentacion = ?", new Object[] { id });
		} catch (Exception e) {
			String descripcionError = String.valueOf(e.getMessage());
			LOG.error(descripcionError);
			throw new CustomerException("0001", internalServerError, "Could not found WorkOrder with id " + id);
		}
		LOG.info("...Fin del metodo deletePresentation...");		
	}

}
