package com.pe.dcptc.webportal.dao.impl;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.pe.dcptc.webportal.canonic.Pagination;
import com.pe.dcptc.webportal.canonic.Provider;
import com.pe.dcptc.webportal.canonic.ProviderCreate;
import com.pe.dcptc.webportal.canonic.ProviderDetail;
import com.pe.dcptc.webportal.canonic.ProviderList;
import com.pe.dcptc.webportal.dao.IProviderDAO;
import com.pe.dcptc.webportal.dao.rowmapper.ProviderDetailRowMapper;
import com.pe.dcptc.webportal.dao.rowmapper.ProviderListRowMapper;
import com.pe.dcptc.webportal.exceptions.CustomerException;
import com.pe.dcptc.webportal.utilitario.Paginado;

@Component
public class ProviderDAO implements IProviderDAO{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	private static final Logger LOG = Logger.getLogger(ClientDAO.class);

	Gson gson = new Gson();

	HttpStatus internalServerError = HttpStatus.INTERNAL_SERVER_ERROR;
	HttpStatus notFound = HttpStatus.NOT_FOUND;
	
	@Transactional
	@Override
	public ProviderDetail getProvider(Integer id) {
		ProviderDetail providerDetail = new ProviderDetail();
		try {
			LOG.info("...Inicio del metodo getProvider...");
			providerDetail = (ProviderDetail) jdbcTemplate.queryForObject("SELECT * FROM PROVEEDORES WHERE nin_id_proveedor = ?",
					new Object[] { id }, new ProviderDetailRowMapper());			
		}catch (Exception e) {
			String descriptionError = String.valueOf(e.getMessage());
			LOG.error(descriptionError);
			throw new CustomerException("001", internalServerError, "Could not found ProgramIncrement with id " + id);
		}
		String JSON = gson.toJson(providerDetail);
		LOG.info("response: " + JSON);
		LOG.info("...Fin del metodo getProvider...");
		
		return providerDetail;
	}
	
	@Transactional
	@Override
	public Provider listProvider(Integer pagina, Integer registrosPagina) {
		Provider provider = new Provider();
		List<ProviderList> providerList = new ArrayList<>();
		try {
			LOG.info("...Inicio del metodo listProvider...");
			providerList = (List<ProviderList>) jdbcTemplate.query(
					"select * from PROVEEDORES" + Paginado.tramaPaginacion(pagina, registrosPagina),
					new ProviderListRowMapper());
			
			Integer totalElementos = jdbcTemplate.queryForObject("SELECT count(*) from PROVEEDORES", Integer.class);
			
			Pagination pagination = new Pagination();
			pagination.setPage(pagina);
			pagination.setPageSize(registrosPagina);
			pagination.setTotalPages((totalElementos / registrosPagina) + 1);
			pagination.setTotalElements(totalElementos);

			provider.setProviderList(providerList);
			provider.setPagination(pagination);
			
		} catch (Exception e) {
			String descripcionError = String.valueOf(e.getMessage());
			LOG.error(descripcionError);
			throw new CustomerException("0001", internalServerError, "Internal error in database");
		}
		String JSON = gson.toJson(providerList);
		LOG.info("response: " + JSON);
		LOG.info("...Fin del metodo listProvider...");
		return provider;
	}

	@Override
	public ProviderCreate createProvider(ProviderCreate provider) {
		try {
			LOG.info("...Inicio del metodo createProvider...");
			KeyHolder keyHolder = new GeneratedKeyHolder();
			String query = "INSERT INTO PROVEEDORES (nin_id_proveedor, txt_nombre_proveedor, txt_sector_proveedor, txt_documento_proveedor, num_num_documento_proveedor, txt_direccion_proveedor, num_telefono_proveedor, txt_ciudad_proveedor, txt_url_proveedor) VALUES (?,?,?,?,?,?,?,?,?)";
			jdbcTemplate.update(connection -> {
				PreparedStatement ps = connection.prepareStatement(query);
				ps.setInt(1, provider.getId());
				ps.setString(2, provider.getName());
				ps.setString(3, provider.getSector());
				ps.setString(4, provider.getDocument());
				ps.setInt(5, provider.getDocumentNumber());
				ps.setString(6, provider.getAddress());
				ps.setInt(7, provider.getPhone());
				ps.setString(8, provider.getCity());
				ps.setString(9, provider.getUrl());
				return ps;
			}, keyHolder);
		} catch (Exception e) {
			String descripcionError = String.valueOf(e.getMessage());
			LOG.error(descripcionError);
			throw new CustomerException("0001", notFound, "There is a null parameter");
		}
		String JSON = gson.toJson(provider);
		LOG.info("response:" + JSON);
		LOG.info("...Fin del metodo createProvider...");
		return provider;
	}

	@Override
	public ProviderCreate updateProvider(ProviderCreate provider, Integer id) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		try {
			LOG.info("...Inicio del metodo updateProvider...");
			String query = "UPDATE PROVEEDORES SET txt_nombre_proveedor=?, txt_sector_proveedor=?, txt_documento_proveedor=?, num_num_documento_proveedor=?, txt_direccion_proveedor=?, num_telefono_proveedor=?, txt_ciudad_proveedor=?, txt_url_proveedor=? WHERE nin_id_proveedor=?";
			jdbcTemplate.update(connection -> {
				PreparedStatement ps = connection.prepareStatement(query);
				ps.setString(1, provider.getName());
				ps.setString(2, provider.getSector());
				ps.setString(3, provider.getDocument());
				ps.setInt(4, provider.getDocumentNumber());
				ps.setString(5, provider.getAddress());
				ps.setInt(6, provider.getPhone());
				ps.setString(7, provider.getCity());
				ps.setString(8, provider.getUrl());
				ps.setInt(9, id);
				return ps;
		}, keyHolder);
	} catch (Exception e) {
		String descripcionError = String.valueOf(e.getMessage());
		LOG.error(descripcionError);
		throw new CustomerException("0001", notFound, "There is a null parameter");
	}
		String JSON = gson.toJson(provider);
		LOG.info("response:" + JSON);
		LOG.info("...Fin del metodo updateProvider...");
		return provider;
	}

	@Override
	public void deleteClient(Integer id) {
		try {
			LOG.info("...Inicio del metodo deleteProvider...");
			jdbcTemplate.update("DELETE FROM PROVEEDORES WHERE nin_id_proveedor = ?", new Object[] { id });

		} catch (Exception e) {
			String descripcionError = String.valueOf(e.getMessage());
			LOG.error(descripcionError);
			throw new CustomerException("0001", internalServerError, "Could not found WorkOrder with id " + id);
		}
		LOG.info("...Fin del metodo deleteProvider...");
	}
	
}
