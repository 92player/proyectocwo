package com.pe.dcptc.webportal.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.pe.dcptc.webportal.canonic.CategoryDetail;

public class CategoryDetailRowMapper implements RowMapper<CategoryDetail>{

	@Override
	public CategoryDetail mapRow(ResultSet rs, int rowNum) throws SQLException {

		CategoryDetail categoryDetail = new CategoryDetail();
		
		categoryDetail.setId(rs.getInt("nin_id_categoria"));
		categoryDetail.setName(rs.getString("txt_nombre_categoria"));
		categoryDetail.setDescription(rs.getString("txt_descripcion_categoria"));
		
		return categoryDetail;
	}

}
