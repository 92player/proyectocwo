package com.pe.dcptc.webportal.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.pe.dcptc.webportal.canonic.CategoryList;

public class CategoryListlRowMapper implements RowMapper<CategoryList>{

	@Override
	public CategoryList mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		CategoryList categoryList = new CategoryList();
		
		categoryList.setId(rs.getInt("nin_id_categoria"));
		categoryList.setName(rs.getString("txt_nombre_categoria"));
		categoryList.setDescription(rs.getString("txt_descripcion_categoria"));
		
		return categoryList;
	}

}
