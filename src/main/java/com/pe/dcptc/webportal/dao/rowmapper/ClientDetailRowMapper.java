package com.pe.dcptc.webportal.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.pe.dcptc.webportal.canonic.ClientDetail;

public class ClientDetailRowMapper implements RowMapper<ClientDetail>{

	@Override
	public ClientDetail mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		ClientDetail clientDetail = new ClientDetail();
		
		clientDetail.setId(rs.getInt("nin_id_cliente"));
		clientDetail.setName(rs.getString("txt_nombre_cliente"));
		clientDetail.setLastName(rs.getString("txt_apellido_cliente"));
		clientDetail.setSexo(rs.getString("txt_sexo_cliente"));
		clientDetail.setDateBirth(rs.getDate("fec_nacimiento_cliente"));
		clientDetail.setdocument(rs.getString("txt_documento_cliente"));
		clientDetail.setDocumentNumber(rs.getInt("num_numero_documento_cliente"));
		clientDetail.setAddress(rs.getString("txt_address_cliente"));
		clientDetail.setCity(rs.getString("txt_ciudad_cliente"));
		clientDetail.setPhone(rs.getInt("num_telefono_cliente"));
		clientDetail.setEmail(rs.getString("txt_email_cliente"));
		
		return clientDetail;
	}

}
