package com.pe.dcptc.webportal.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.pe.dcptc.webportal.canonic.ClientList;

public class ClientListRowMapper implements RowMapper<ClientList>{
		
	@Override
	public ClientList mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		ClientList clientList = new ClientList();
		
		clientList.setId(rs.getInt("nin_id_cliente"));
		clientList.setName(rs.getString("txt_nombre_cliente"));
		clientList.setLastName(rs.getString("txt_apellido_cliente"));
		clientList.setSexo(rs.getString("txt_sexo_cliente"));
		clientList.setDateBirth(rs.getDate("fec_nacimiento_cliente"));
		clientList.setdocument(rs.getString("txt_documento_cliente"));
		clientList.setDocumentNumber(rs.getInt("num_numero_documento_cliente"));
		clientList.setAddress(rs.getString("txt_address_cliente"));
		clientList.setCity(rs.getString("txt_ciudad_cliente"));
		clientList.setPhone(rs.getInt("num_telefono_cliente"));
		clientList.setEmail(rs.getString("txt_email_cliente"));
		
		return clientList;
	}

}
