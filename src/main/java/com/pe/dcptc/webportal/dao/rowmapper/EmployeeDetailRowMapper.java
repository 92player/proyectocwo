package com.pe.dcptc.webportal.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.pe.dcptc.webportal.canonic.EmployeeDetail;

public class EmployeeDetailRowMapper implements RowMapper<EmployeeDetail> {

	@Override
	public EmployeeDetail mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		EmployeeDetail employeeDetail = new EmployeeDetail();

		employeeDetail.setId(rs.getInt("nin_id_empleado"));
		employeeDetail.setName(rs.getString("txt_nombre_empleado"));
		employeeDetail.setLastName(rs.getString("txt_apellido_empleado"));
		employeeDetail.setSexo(rs.getString("txt_sexo_empleado"));
		employeeDetail.setDateBirth(rs.getDate("fec_nacimiento_empleado"));
		employeeDetail.setDocumentNumber(rs.getInt("num_numero_documento_empleado"));
		employeeDetail.setAddress(rs.getString("txt_address_empleado"));
		employeeDetail.setPhone(rs.getInt("num_telefono_empleado"));
		employeeDetail.setEmail(rs.getString("txt_email_empleado"));
		employeeDetail.setAcces(rs.getString("txt_acceso_empleado"));
		employeeDetail.setUser(rs.getString("txt_usuario_empleado"));
		employeeDetail.setPassword(rs.getString("txt_contrasena_empleado"));
		
		return employeeDetail;
	}

}
