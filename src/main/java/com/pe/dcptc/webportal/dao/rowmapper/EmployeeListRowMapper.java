package com.pe.dcptc.webportal.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.pe.dcptc.webportal.canonic.EmployeeList;

public class EmployeeListRowMapper implements RowMapper<EmployeeList>{

	@Override
	public EmployeeList mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		EmployeeList employeeList = new EmployeeList();
		
		employeeList.setId(rs.getInt("nin_id_empleado"));
		employeeList.setName(rs.getString("txt_nombre_empleado"));
		employeeList.setLastName(rs.getString("txt_apellido_empleado"));
		employeeList.setSexo(rs.getString("txt_sexo_empleado"));
		employeeList.setDateBirth(rs.getDate("fec_nacimiento_empleado"));
		employeeList.setDocumentNumber(rs.getInt("num_numero_documento_empleado"));
		employeeList.setAddress(rs.getString("txt_address_empleado"));
		employeeList.setPhone(rs.getInt("num_telefono_empleado"));
		employeeList.setEmail(rs.getString("txt_email_empleado"));
		employeeList.setAcces(rs.getString("txt_acceso_empleado"));
		employeeList.setUser(rs.getString("txt_usuario_empleado"));
		employeeList.setPassword(rs.getString("txt_contrasena_empleado"));
	
		return employeeList;
	}

}
