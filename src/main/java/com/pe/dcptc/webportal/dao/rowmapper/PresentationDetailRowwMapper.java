package com.pe.dcptc.webportal.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.pe.dcptc.webportal.canonic.PresentationDetail;

public class PresentationDetailRowwMapper implements RowMapper<PresentationDetail>{

	@Override
	public PresentationDetail mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		PresentationDetail presentationDetail = new PresentationDetail();
		
		presentationDetail.setId(rs.getInt("nin_id_presentacion"));
		presentationDetail.setName(rs.getString("txt_nombre_presentacion"));
		presentationDetail.setDescription(rs.getString("txt_descripcion_presentacion"));
		
		return presentationDetail;
	}

}
