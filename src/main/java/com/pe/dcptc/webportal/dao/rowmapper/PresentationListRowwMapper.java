package com.pe.dcptc.webportal.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.pe.dcptc.webportal.canonic.PresentationList;

public class PresentationListRowwMapper implements RowMapper<PresentationList>{

	@Override
	public PresentationList mapRow(ResultSet rs, int rowNum) throws SQLException {

		PresentationList presentationList = new PresentationList();
		
		presentationList.setId(rs.getInt("nin_id_presentacion"));
		presentationList.setName(rs.getString("txt_nombre_presentacion"));
		presentationList.setDescription(rs.getString("txt_descripcion_presentacion"));
		
		return presentationList;
	}

}
