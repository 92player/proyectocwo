package com.pe.dcptc.webportal.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.pe.dcptc.webportal.canonic.ProviderDetail;

public class ProviderDetailRowMapper implements RowMapper<ProviderDetail>{

	@Override
	public ProviderDetail mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		ProviderDetail providerDetail = new ProviderDetail();
		
		providerDetail.setId(rs.getInt("nin_id_proveedor"));
		providerDetail.setName(rs.getString("txt_nombre_proveedor"));
		providerDetail.setSector(rs.getString("txt_sector_proveedor"));
		providerDetail.setDocument(rs.getString("txt_documento_proveedor"));
		providerDetail.setDocumentNumber(rs.getInt("num_num_documento_proveedor"));
		providerDetail.setAddress(rs.getString("txt_direccion_proveedor"));
		providerDetail.setPhone(rs.getInt("num_telefono_proveedor"));
		providerDetail.setCity(rs.getString("txt_ciudad_proveedor"));
		providerDetail.setUrl(rs.getString("txt_url_proveedor"));
		
		return providerDetail;
	}

}
