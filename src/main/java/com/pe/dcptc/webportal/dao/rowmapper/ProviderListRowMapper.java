package com.pe.dcptc.webportal.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.pe.dcptc.webportal.canonic.ProviderList;

public class ProviderListRowMapper implements RowMapper<ProviderList>{

	@Override
	public ProviderList mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		ProviderList providerList = new ProviderList();
		
		providerList.setId(rs.getInt("nin_id_proveedor"));
		providerList.setName(rs.getString("txt_nombre_proveedor"));
		providerList.setSector(rs.getString("txt_sector_proveedor"));
		providerList.setDocument(rs.getString("txt_documento_proveedor"));
		providerList.setDocumentNumber(rs.getInt("num_num_documento_proveedor"));
		providerList.setAddress(rs.getString("txt_direccion_proveedor"));
		providerList.setPhone(rs.getInt("num_telefono_proveedor"));
		providerList.setCity(rs.getString("txt_ciudad_proveedor"));
		providerList.setUrl(rs.getString("txt_url_proveedor"));

		return providerList;
	}

}
