package com.pe.dcptc.webportal.exceptions;

import org.springframework.http.HttpStatus;

public class CustomerException extends GenericExceptionBase{

	private static final long serialVersionUID = -7417720787588739139L;
	
	public CustomerException (String code, HttpStatus statusCode,String message) {
		
		super(code, statusCode, message);
		
	}

}
