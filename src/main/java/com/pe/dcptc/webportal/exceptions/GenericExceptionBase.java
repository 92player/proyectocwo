package com.pe.dcptc.webportal.exceptions;

import org.springframework.http.HttpStatus;

public class GenericExceptionBase extends RuntimeException{

	private static final long serialVersionUID = -3260403559235057676L;
	
	private String code;
	private HttpStatus statusCode;
	
	public GenericExceptionBase(String code,HttpStatus statusCode, String message) {
		
		super(message);
	    this.code = code;
	    this.statusCode = statusCode;
	    
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public HttpStatus getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(HttpStatus statusCode) {
		this.statusCode = statusCode;
	}

}
