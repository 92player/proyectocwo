package com.pe.dcptc.webportal.facade.v0;

import com.pe.dcptc.webportal.canonic.CategoryCreate;
import com.pe.dcptc.webportal.utilitario.Response;
import com.pe.dcptc.webportal.utilitario.ResponsePagination;

public interface ICategoryControllerV0 {
	
	Response getCategory (Integer id);
	ResponsePagination listCategory(Integer pagina,Integer registrosPagina);
	CategoryCreate createCategory (CategoryCreate category);
	CategoryCreate updateCategory (CategoryCreate category, Integer id);
	void deleteCategory (Integer id);

}
