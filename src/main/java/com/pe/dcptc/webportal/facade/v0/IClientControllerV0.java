package com.pe.dcptc.webportal.facade.v0;

import com.pe.dcptc.webportal.canonic.ClientCreate;
import com.pe.dcptc.webportal.utilitario.Response;
import com.pe.dcptc.webportal.utilitario.ResponsePagination;

public interface IClientControllerV0 {
	
	Response getClient (Integer id);
	ResponsePagination listClient(Integer pagina,Integer registrosPagina);
	ClientCreate createClient (ClientCreate client);
	ClientCreate updateClient (ClientCreate client, Integer id);
	void deleteClient (Integer id);

}
