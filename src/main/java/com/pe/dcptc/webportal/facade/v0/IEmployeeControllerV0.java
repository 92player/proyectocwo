package com.pe.dcptc.webportal.facade.v0;

import com.pe.dcptc.webportal.canonic.EmployeeCreate;
import com.pe.dcptc.webportal.utilitario.Response;
import com.pe.dcptc.webportal.utilitario.ResponsePagination;

public interface IEmployeeControllerV0 {
	
	Response getEmployee (Integer id);
	ResponsePagination listEmployee(Integer pagina,Integer registrosPagina);
	EmployeeCreate createEmployee (EmployeeCreate employee);
	EmployeeCreate updateEmployee (EmployeeCreate employee, Integer id);
	void deleteEmployee (Integer id);

}
