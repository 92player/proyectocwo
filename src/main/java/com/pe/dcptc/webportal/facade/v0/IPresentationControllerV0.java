package com.pe.dcptc.webportal.facade.v0;

import com.pe.dcptc.webportal.canonic.PresentationCreate;
import com.pe.dcptc.webportal.utilitario.Response;
import com.pe.dcptc.webportal.utilitario.ResponsePagination;

public interface IPresentationControllerV0 {
	
	Response getPresentation (Integer id);
	ResponsePagination listPresentation(Integer pagina,Integer registrosPagina);
	PresentationCreate createPresentation (PresentationCreate presentation);
	PresentationCreate updatePresentation (PresentationCreate presentation, Integer id);
	void deletePresentation (Integer id);

}
