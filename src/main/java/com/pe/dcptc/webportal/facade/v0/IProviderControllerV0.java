package com.pe.dcptc.webportal.facade.v0;

import com.pe.dcptc.webportal.canonic.ProviderCreate;
import com.pe.dcptc.webportal.utilitario.Response;
import com.pe.dcptc.webportal.utilitario.ResponsePagination;

public interface IProviderControllerV0 {
	
	Response getProvider (Integer id);
	ResponsePagination listProvider(Integer pagina,Integer registrosPagina);
	ProviderCreate createProvider (ProviderCreate provider);
	ProviderCreate updateProvider (ProviderCreate provider, Integer	id);
	void deleteProvider(Integer id);
	
}
