package com.pe.dcptc.webportal.facade.v0.impl;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import com.pe.dcptc.webportal.bussines.ICategoryService;
import com.pe.dcptc.webportal.canonic.Category;
import com.pe.dcptc.webportal.canonic.CategoryCreate;
import com.pe.dcptc.webportal.canonic.CategoryDetail;
import com.pe.dcptc.webportal.canonic.Link;
import com.pe.dcptc.webportal.canonic.Pagination;
import com.pe.dcptc.webportal.facade.v0.ICategoryControllerV0;
import com.pe.dcptc.webportal.utilitario.Response;
import com.pe.dcptc.webportal.utilitario.ResponsePagination;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/category/v0")
public class CategoryControllerV0 implements ICategoryControllerV0 {

	@Autowired
	private ICategoryService iCategoryService;

	private static final Logger LOG = Logger.getLogger(CategoryControllerV0.class);

	@RequestMapping(value = "/category/{id}", method = RequestMethod.GET)
	@Override
	public @ResponseBody Response getCategory(@PathVariable Integer id) {
		LOG.info("...Inicio del Servicio getCategory...");
		Response response = new Response();
		CategoryDetail CategoryDetail = iCategoryService.getCategory(id);
		response.setData(CategoryDetail);
		return response;
	}

	@RequestMapping(value = "/category/list/", method = RequestMethod.GET)
	@Override
	public @ResponseBody ResponsePagination listCategory(@RequestParam(name = "paginationKey") Integer pagina,
			@RequestParam(name = "pageSize") Integer registrosPagina) {
		LOG.info("...Inicio del Servicio listCategory...");
		ResponsePagination response = new ResponsePagination();
		Category Category = iCategoryService.listCategory(pagina, registrosPagina);
		Pagination pagination = Category.getPagination();
		Link link = new Link();

		UriComponentsBuilder builder = ServletUriComponentsBuilder.fromCurrentRequest();
		String requestedValue = builder.buildAndExpand().getPath();

		UriComponentsBuilder firstLink = ServletUriComponentsBuilder.fromUriString(requestedValue)
				.queryParam("paginationKey", 1).queryParam("pageSize", registrosPagina);
		String linkFirst = firstLink.toUriString();

		UriComponentsBuilder lastLink = ServletUriComponentsBuilder.fromUriString(requestedValue)
				.queryParam("paginationKey", pagination.getTotalPages()).queryParam("pageSize", registrosPagina);
		String linkLast = lastLink.toUriString();

		UriComponentsBuilder previosLink = ServletUriComponentsBuilder.fromUriString(requestedValue)
				.queryParam("paginationKey", pagina - 1).queryParam("pageSize", registrosPagina);
		String linkPrevios = previosLink.toUriString();

		UriComponentsBuilder nextLink = ServletUriComponentsBuilder.fromUriString(requestedValue)
				.queryParam("paginationKey", pagina + 1).queryParam("pageSize", registrosPagina);
		String linkNext = nextLink.toUriString();

		link.setLinksFirst(linkFirst);
		link.setLinksLast(linkLast);
		link.setLinksPrevios(linkPrevios);
		link.setLinksNext(linkNext);
		pagination.setLinks(link);
		Category.setPagination(pagination);

		response.setData(Category.getCategoryList());
		response.setPagination(Category.getPagination());
		return response;
		
	}

	@Override
	@PostMapping(value = "/category")
	@ResponseStatus(code = HttpStatus.CREATED)
	public CategoryCreate createCategory(@Valid @RequestBody CategoryCreate Category) {
		LOG.info("...Inicio del servicio createCategory...");
		CategoryCreate CategoryCreate = iCategoryService.createCategory(Category);
		return CategoryCreate;
	}

	@Override
	@PutMapping(value = "/category/{id}")
	public CategoryCreate updateCategory(@RequestBody CategoryCreate Category, @PathVariable Integer id) {
		LOG.info("..Inicio del servicio updateCategory...");
		return iCategoryService.updateCategory(Category, id);
	}

	@Override
	@DeleteMapping(value = "/category/{id}")
	public void deleteCategory(@PathVariable Integer id) {
		LOG.info("...Inicio del servicio deleteCategory...");
		iCategoryService.deleteCategory(id);
	}

}
