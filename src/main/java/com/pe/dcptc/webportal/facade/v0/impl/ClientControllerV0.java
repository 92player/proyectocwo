package com.pe.dcptc.webportal.facade.v0.impl;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import com.pe.dcptc.webportal.bussines.IClientService;
import com.pe.dcptc.webportal.canonic.Client;
import com.pe.dcptc.webportal.canonic.ClientCreate;
import com.pe.dcptc.webportal.canonic.ClientDetail;
import com.pe.dcptc.webportal.canonic.Link;
import com.pe.dcptc.webportal.canonic.Pagination;
import com.pe.dcptc.webportal.facade.v0.IClientControllerV0;
import com.pe.dcptc.webportal.utilitario.Response;
import com.pe.dcptc.webportal.utilitario.ResponsePagination;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/client/v0")
public class ClientControllerV0 implements IClientControllerV0 {

	@Autowired
	private IClientService iClientService;

	private static final Logger LOG = Logger.getLogger(ClientControllerV0.class);

	@RequestMapping(value = "/client/{id}", method = RequestMethod.GET)
	@Override
	public @ResponseBody Response getClient(@PathVariable Integer id) {
		LOG.info("...Inicio del Servicio getClient...");
		Response response = new Response();
		ClientDetail clientDetail = iClientService.getClient(id);
		response.setData(clientDetail);
		return response;
	}

	@RequestMapping(value = "/client/list/", method = RequestMethod.GET)
	@Override
	public @ResponseBody ResponsePagination listClient(@RequestParam(name = "paginationKey") Integer pagina,
			@RequestParam(name = "pageSize") Integer registrosPagina) {
		LOG.info("...Inicio del Servicio listClient...");
		ResponsePagination response = new ResponsePagination();
		Client client = iClientService.listClient(pagina, registrosPagina);
		Pagination pagination = client.getPagination();
		Link link = new Link();

		UriComponentsBuilder builder = ServletUriComponentsBuilder.fromCurrentRequest();
		String requestedValue = builder.buildAndExpand().getPath();

		UriComponentsBuilder firstLink = ServletUriComponentsBuilder.fromUriString(requestedValue)
				.queryParam("paginationKey", 1).queryParam("pageSize", registrosPagina);
		String linkFirst = firstLink.toUriString();

		UriComponentsBuilder lastLink = ServletUriComponentsBuilder.fromUriString(requestedValue)
				.queryParam("paginationKey", pagination.getTotalPages()).queryParam("pageSize", registrosPagina);
		String linkLast = lastLink.toUriString();

		UriComponentsBuilder previosLink = ServletUriComponentsBuilder.fromUriString(requestedValue)
				.queryParam("paginationKey", pagina - 1).queryParam("pageSize", registrosPagina);
		String linkPrevios = previosLink.toUriString();

		UriComponentsBuilder nextLink = ServletUriComponentsBuilder.fromUriString(requestedValue)
				.queryParam("paginationKey", pagina + 1).queryParam("pageSize", registrosPagina);
		String linkNext = nextLink.toUriString();

		link.setLinksFirst(linkFirst);
		link.setLinksLast(linkLast);
		link.setLinksPrevios(linkPrevios);
		link.setLinksNext(linkNext);
		pagination.setLinks(link);
		client.setPagination(pagination);

		response.setData(client.getClientList());
		response.setPagination(client.getPagination());
		return response;
		
	}

	@Override
	@PostMapping(value = "/client")
	@ResponseStatus(code = HttpStatus.CREATED)
	public ClientCreate createClient(@Valid @RequestBody ClientCreate client) {
		LOG.info("...Inicio del servicio createClient...");
		ClientCreate clientCreate = iClientService.createClient(client);
		return clientCreate;
	}

	@Override
	@PutMapping(value = "/client/{id}")
	public ClientCreate updateClient(@RequestBody ClientCreate client, @PathVariable Integer id) {
		LOG.info("..Inicio del servicio updateClient...");
		return iClientService.updateClient(client, id);
	}

	@Override
	@DeleteMapping(value = "/client/{id}")
	public void deleteClient(@PathVariable Integer id) {
		LOG.info("...Inicio del servicio deleteClient...");
		iClientService.deleteClient(id);
	}

}
