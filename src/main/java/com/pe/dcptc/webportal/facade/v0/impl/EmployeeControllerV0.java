package com.pe.dcptc.webportal.facade.v0.impl;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import com.pe.dcptc.webportal.bussines.IEmployeeService;
import com.pe.dcptc.webportal.canonic.Employee;
import com.pe.dcptc.webportal.canonic.EmployeeCreate;
import com.pe.dcptc.webportal.canonic.EmployeeDetail;
import com.pe.dcptc.webportal.canonic.Link;
import com.pe.dcptc.webportal.canonic.Pagination;
import com.pe.dcptc.webportal.facade.v0.IEmployeeControllerV0;
import com.pe.dcptc.webportal.utilitario.Response;
import com.pe.dcptc.webportal.utilitario.ResponsePagination;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/employee/v0")
public class EmployeeControllerV0 implements IEmployeeControllerV0{
	
	@Autowired
	private IEmployeeService iEmployeeService;

	private static final Logger LOG = Logger.getLogger(EmployeeControllerV0.class);

	@RequestMapping(value = "/employee/{id}", method = RequestMethod.GET)
	@Override
	public @ResponseBody Response getEmployee(@PathVariable Integer id) {
		LOG.info("...Inicio del Servicio getEmployee...");
		Response response = new Response();
		EmployeeDetail employeeDetail = iEmployeeService.getEmployee(id);
		response.setData(employeeDetail);
		return response;
	}
	
	@RequestMapping(value = "/employee/list/", method = RequestMethod.GET)
	@Override
	public @ResponseBody ResponsePagination listEmployee(@RequestParam(name = "paginationKey") Integer pagina,
			@RequestParam(name = "pageSize") Integer registrosPagina) {
		LOG.info("...Inicio del Servicio listEmployee...");
		ResponsePagination response = new ResponsePagination();
		Employee employee = iEmployeeService.listEmployee(pagina, registrosPagina);
		Pagination pagination = employee.getPagination();
		Link link = new Link();
		
		UriComponentsBuilder builder = ServletUriComponentsBuilder.fromCurrentRequest();
		String requestedValue = builder.buildAndExpand().getPath();

		UriComponentsBuilder firstLink = ServletUriComponentsBuilder.fromUriString(requestedValue)
				.queryParam("paginationKey", 1).queryParam("pageSize", registrosPagina);
		String linkFirst = firstLink.toUriString();

		UriComponentsBuilder lastLink = ServletUriComponentsBuilder.fromUriString(requestedValue)
				.queryParam("paginationKey", pagination.getTotalPages()).queryParam("pageSize", registrosPagina);
		String linkLast = lastLink.toUriString();

		UriComponentsBuilder previosLink = ServletUriComponentsBuilder.fromUriString(requestedValue)
				.queryParam("paginationKey", pagina - 1).queryParam("pageSize", registrosPagina);
		String linkPrevios = previosLink.toUriString();

		UriComponentsBuilder nextLink = ServletUriComponentsBuilder.fromUriString(requestedValue)
				.queryParam("paginationKey", pagina + 1).queryParam("pageSize", registrosPagina);
		String linkNext = nextLink.toUriString();
		
		link.setLinksFirst(linkFirst);
		link.setLinksLast(linkLast);
		link.setLinksPrevios(linkPrevios);
		link.setLinksNext(linkNext);
		pagination.setLinks(link);
		employee.setPagination(pagination);

		response.setData(employee.getEmployeeList());
		response.setPagination(employee.getPagination());
		return response;
		
	}

	@Override
	@PostMapping(value = "/employee")
	@ResponseStatus(code = HttpStatus.CREATED)
	public EmployeeCreate createEmployee(@Valid @RequestBody EmployeeCreate employee) {
		LOG.info("...Inicio del servicio createEmployee...");
		EmployeeCreate employeeCreate = iEmployeeService.createEmployee(employee);
		return employeeCreate;
	}

	@Override
	@PutMapping(value = "/employee/{id}")
	public EmployeeCreate updateEmployee(@RequestBody EmployeeCreate employee, @PathVariable Integer id) {
		LOG.info("..Inicio del servicio updateClient...");
		return iEmployeeService.updateEmployee(employee, id);
	}

	@Override
	@DeleteMapping(value = "/employee/{id}")
	public void deleteEmployee(@PathVariable Integer id) {
		LOG.info("...Inicio del servicio deleteEmployee...");
		iEmployeeService.deleteEmployee(id);
	}

}
