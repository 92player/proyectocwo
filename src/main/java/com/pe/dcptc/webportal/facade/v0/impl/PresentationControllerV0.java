package com.pe.dcptc.webportal.facade.v0.impl;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import com.pe.dcptc.webportal.bussines.IPresentationService;
import com.pe.dcptc.webportal.canonic.Link;
import com.pe.dcptc.webportal.canonic.Pagination;
import com.pe.dcptc.webportal.canonic.Presentation;
import com.pe.dcptc.webportal.canonic.PresentationCreate;
import com.pe.dcptc.webportal.canonic.PresentationDetail;
import com.pe.dcptc.webportal.facade.v0.IPresentationControllerV0;
import com.pe.dcptc.webportal.utilitario.Response;
import com.pe.dcptc.webportal.utilitario.ResponsePagination;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/presentation/v0")
public class PresentationControllerV0 implements IPresentationControllerV0{
	
	@Autowired
	private IPresentationService iPresentationService;
	
	private static final Logger LOG = Logger.getLogger(PresentationControllerV0.class);
	
	@RequestMapping(value = "/presentation/{id}", method = RequestMethod.GET)
	@Override
	public @ResponseBody Response getPresentation(@PathVariable Integer id) {
		LOG.info("...Inicio del Servicio getPresentacion...");
		Response response = new Response();
		PresentationDetail presentationDetail = iPresentationService.getPresentation(id);
		response.setData(presentationDetail);
		return response;
	}
	
	@RequestMapping(value = "/presentation/list/", method = RequestMethod.GET)
	@Override
	public ResponsePagination listPresentation(@RequestParam(name = "paginationKey") Integer pagina,
			@RequestParam(name = "pageSize") Integer registrosPagina) {
		LOG.info("...Inicio del Servicio listPresentation...");
		ResponsePagination response = new ResponsePagination();
		Presentation presentation = iPresentationService.listPresentation(pagina, registrosPagina);
		Pagination pagination = presentation.getPagination();
		Link link = new Link();
		
		UriComponentsBuilder builder = ServletUriComponentsBuilder.fromCurrentRequest();
		String requestedValue = builder.buildAndExpand().getPath();

		UriComponentsBuilder firstLink = ServletUriComponentsBuilder.fromUriString(requestedValue)
				.queryParam("paginationKey", 1).queryParam("pageSize", registrosPagina);
		String linkFirst = firstLink.toUriString();

		UriComponentsBuilder lastLink = ServletUriComponentsBuilder.fromUriString(requestedValue)
				.queryParam("paginationKey", pagination.getTotalPages()).queryParam("pageSize", registrosPagina);
		String linkLast = lastLink.toUriString();

		UriComponentsBuilder previosLink = ServletUriComponentsBuilder.fromUriString(requestedValue)
				.queryParam("paginationKey", pagina - 1).queryParam("pageSize", registrosPagina);
		String linkPrevios = previosLink.toUriString();

		UriComponentsBuilder nextLink = ServletUriComponentsBuilder.fromUriString(requestedValue)
				.queryParam("paginationKey", pagina + 1).queryParam("pageSize", registrosPagina);
		String linkNext = nextLink.toUriString();
		
		link.setLinksFirst(linkFirst);
		link.setLinksLast(linkLast);
		link.setLinksPrevios(linkPrevios);
		link.setLinksNext(linkNext);
		pagination.setLinks(link);
		presentation.setPagination(pagination);
		
		response.setData(presentation.getPresentationList());
		response.setPagination(presentation.getPagination());
		return response;
	}

	@Override
	@PostMapping(value = "/presentation")
	@ResponseStatus(code = HttpStatus.CREATED)
	public PresentationCreate createPresentation(@Valid @RequestBody PresentationCreate presentation) {
		LOG.info("...Inicio del servicio createPresentation...");
		PresentationCreate presentationCreate = iPresentationService.createPresentation(presentation);
		return presentationCreate;		

	}

	@Override
	@PutMapping(value = "/presentation/{id}")
	public PresentationCreate updatePresentation(@RequestBody PresentationCreate presentation, @PathVariable Integer id) {
		LOG.info("..Inicio del servicio updatePresentation...");
		return iPresentationService.updatePresentation(presentation, id);
	}

	@Override
	@DeleteMapping(value = "/presentation/{id}")
	public void deletePresentation(@PathVariable Integer id) {
		LOG.info("...Inicio del servicio deletePresentation...");
		iPresentationService.deletePresentation(id);
	}

}
