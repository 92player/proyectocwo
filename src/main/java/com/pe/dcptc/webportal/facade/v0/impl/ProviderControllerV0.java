package com.pe.dcptc.webportal.facade.v0.impl;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import com.pe.dcptc.webportal.bussines.IProviderService;
import com.pe.dcptc.webportal.canonic.Link;
import com.pe.dcptc.webportal.canonic.Pagination;
import com.pe.dcptc.webportal.canonic.Provider;
import com.pe.dcptc.webportal.canonic.ProviderCreate;
import com.pe.dcptc.webportal.canonic.ProviderDetail;
import com.pe.dcptc.webportal.facade.v0.IProviderControllerV0;
import com.pe.dcptc.webportal.utilitario.Response;
import com.pe.dcptc.webportal.utilitario.ResponsePagination;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/provider/v0")
public class ProviderControllerV0 implements IProviderControllerV0{
	
	@Autowired
	private IProviderService iProviderService;

	private static final Logger LOG = Logger.getLogger(ProviderControllerV0.class);
	
	@RequestMapping(value = "/provider/{id}", method = RequestMethod.GET)
	@Override
	public @ResponseBody Response getProvider(@PathVariable Integer id) {
		LOG.info("...Inicio del Servicio getClient...");
		Response response = new Response();
		ProviderDetail providerDetail = iProviderService.getProvider(id);
		response.setData(providerDetail);
		return response;
	}
	
	@RequestMapping(value = "/provider/list/", method = RequestMethod.GET)
	@Override
	public @ResponseBody ResponsePagination listProvider(@RequestParam(name = "paginationKey") Integer pagina,
			@RequestParam(name = "pageSize") Integer registrosPagina){
		LOG.info("...Inicio del Servicio listProvider...");
		ResponsePagination response = new ResponsePagination();
		Provider provider = iProviderService.listProvider(pagina, registrosPagina);
		Pagination pagination = provider.getPagination();
		Link link = new Link();
		
		UriComponentsBuilder builder = ServletUriComponentsBuilder.fromCurrentRequest();
		String requestedValue = builder.buildAndExpand().getPath();

		UriComponentsBuilder firstLink = ServletUriComponentsBuilder.fromUriString(requestedValue)
				.queryParam("paginationKey", 1).queryParam("pageSize", registrosPagina);
		String linkFirst = firstLink.toUriString();

		UriComponentsBuilder lastLink = ServletUriComponentsBuilder.fromUriString(requestedValue)
				.queryParam("paginationKey", pagination.getTotalPages()).queryParam("pageSize", registrosPagina);
		String linkLast = lastLink.toUriString();

		UriComponentsBuilder previosLink = ServletUriComponentsBuilder.fromUriString(requestedValue)
				.queryParam("paginationKey", pagina - 1).queryParam("pageSize", registrosPagina);
		String linkPrevios = previosLink.toUriString();
		
		UriComponentsBuilder nextLink = ServletUriComponentsBuilder.fromUriString(requestedValue)
				.queryParam("paginationKey", pagina + 1).queryParam("pageSize", registrosPagina);
		String linkNext = nextLink.toUriString();

		link.setLinksFirst(linkFirst);
		link.setLinksLast(linkLast);
		link.setLinksPrevios(linkPrevios);
		link.setLinksNext(linkNext);
		pagination.setLinks(link);
		provider.setPagination(pagination);

		response.setData(provider.getProviderList());
		response.setPagination(provider.getPagination());
		return response;
		
	}

	@Override
	@PostMapping(value = "/provider")
	@ResponseStatus(code = HttpStatus.CREATED)
	public ProviderCreate createProvider(@Valid @RequestBody ProviderCreate provider) {
		LOG.info("...Inicio del servicio createProvider...");
		ProviderCreate providerCreate = iProviderService.createProvider(provider);
		return providerCreate;
	}

	@Override
	@PutMapping(value = "/provider/{id}")
	public ProviderCreate updateProvider(@RequestBody ProviderCreate provider, @PathVariable Integer id) {
		LOG.info("..Inicio del servicio updateProvider...");
		return iProviderService.updateProvider(provider, id);
	}

	@Override
	@DeleteMapping(value = "/provider/{id}")
	public void deleteProvider(@PathVariable Integer id) {
		LOG.info("...Inicio del servicio deleteProvider...");
		iProviderService.deleteProvider(id);
	}

}
