package com.pe.dcptc.webportal.utilitario;

public class Paginado {
	
	public static String tramaPaginacion(int pagina, int registrosPagina) {
		Integer valor_1 = registrosPagina;
		Integer valor_2 = valor_1 * (pagina-1);
		// Traer X registros(valor_1)			  desde el registros Y (valor_2) 
		return " OFFSET " +String.valueOf(valor_2)+" ROWS FETCH NEXT "+String.valueOf(valor_1)+ " ROWS ONLY ";
//		return "limit "+String.valueOf(valor_1)+" offset "+String.valueOf(valor_2);
	}
	
}
