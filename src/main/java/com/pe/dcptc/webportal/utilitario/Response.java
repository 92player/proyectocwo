package com.pe.dcptc.webportal.utilitario;

import java.io.Serializable;

public class Response implements Serializable{

	private static final long serialVersionUID = -4179006031286180449L;
	
	private Object  data;

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
}
