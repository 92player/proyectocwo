package com.pe.dcptc.webportal.utilitario;

import java.io.Serializable;

public class ResponsePagination implements Serializable{

	private static final long serialVersionUID = -5452398506465978734L;
	
	private Object data;
	private Object pagination;
	
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public Object getPagination() {
		return pagination;
	}
	public void setPagination(Object pagination) {
		this.pagination = pagination;
	}
	
}
