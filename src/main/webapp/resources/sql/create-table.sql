CREATE TABLE CLIENTE (
	nin_id_cliente INTEGER NOT NULL,
	txt_nombre_cliente varchar(20) NOT NULL,
	txt_apellido_cliente varchar(40) NOT NULL,
	txt_sexo_cliente varchar(1) NOT NULL,
	fec_nacimiento_cliente DATE NOT NULL ,
	txt_documento_cliente varchar(1) NOT NULL,
	num_numero_documento_cliente INTEGER NOT NULL,
	txt_address_cliente varchar(40) NOT NULL,
	txt_ciudad_cliente varchar(20) NOT NULL,
	num_telefono_cliente INTEGER NOT NULL,
	txt_email_cliente varchar(50) NOT NULL,
	PRIMARY KEY (nin_id_cliente)
	);

CREATE TABLE PROVEEDORES (
	nin_id_proveedor INTEGER NOT NULL,
	txt_nombre_proveedor varchar(150) NOT NULL,
    txt_sector_proveedor varchar(50) NOT NULL,
    txt_documento_proveedor varchar(20) NOT NULL,
    num_num_documento_proveedor INTEGER NOT NULL,
    txt_direccion_proveedor varchar(11) NOT NULL,
    num_telefono_proveedor  INTEGER NOT NULL,
    txt_ciudad_proveedor varchar(20) NOT NULL,
    txt_url_proveedor varchar(20) NOT NULL,
	PRIMARY KEY (nin_id_proveedor)
	);
	
CREATE TABLE EMPLEADO (
	nin_id_empleado INTEGER NOT NULL,
	txt_nombre_empleado varchar(20) NOT NULL,
    txt_apellido_empleado varchar(40) NOT NULL,
    txt_sexo_empleado varchar(1) NOT NULL,
    fec_nacimiento_empleado DATE NOT NULL ,
    num_numero_documento_empleado INTEGER NOT NULL,
    txt_address_empleado varchar(100) NOT NULL,
    num_telefono_empleado INTEGER NOT NULL,
    txt_email_empleado varchar(50) NOT NULL,
    txt_acceso_empleado varchar(20) NOT NULL,
    txt_usuario_empleado varchar(20) NOT NULL,
    txt_contrasena_empleado varchar(20) NOT NULL,
	PRIMARY KEY (nin_id_empleado)
	);

CREATE TABLE CATEGORIA (
	nin_id_categoria INTEGER NOT NULL,
	txt_nombre_categoria varchar(50) NOT NULL,
    txt_descripcion_categoria varchar(250) NOT NULL,
	PRIMARY KEY (nin_id_categoria)
	);

CREATE TABLE PRESENTACION (
	nin_id_presentacion INTEGER NOT NULL,
	txt_nombre_presentacion varchar(50) NOT NULL,
    txt_descripcion_presentacion varchar(250) NOT NULL,
	PRIMARY KEY (nin_id_presentacion)
	);
	
