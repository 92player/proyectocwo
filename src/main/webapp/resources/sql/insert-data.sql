INSERT into CLIENTE (nin_id_cliente, txt_nombre_cliente, txt_apellido_cliente, txt_sexo_cliente, fec_nacimiento_cliente, txt_documento_cliente, num_numero_documento_cliente, txt_address_cliente, txt_ciudad_cliente, num_telefono_cliente, txt_email_cliente) VALUES (1, 'Jhordan', 'Miranda', 'M', '1992-08-11', 'D', 48484811, 'Ate - Viatrte', 'Lima', 9988111, 'aldo@gmail.com');

INSERT into PROVEEDORES (nin_id_proveedor, txt_nombre_proveedor, txt_sector_proveedor, txt_documento_proveedor, num_num_documento_proveedor, txt_direccion_proveedor, num_telefono_proveedor, txt_ciudad_proveedor, txt_url_proveedor) VALUES (1, 'Tata', 'Informatica', 'ruc', 111111111, 'El Agustino', 7061111, 'Lima', 'www.tcs.com');

INSERT into EMPLEADO (nin_id_empleado, txt_nombre_empleado, txt_apellido_empleado, txt_sexo_empleado, fec_nacimiento_empleado, num_numero_documento_empleado, txt_address_empleado, num_telefono_empleado, txt_email_empleado, txt_acceso_empleado, txt_usuario_empleado, txt_contrasena_empleado) VALUES (1, 'Aldo', 'Miranda', 'M', '1992-08-11', 48051111, 'Ate Vitarte', 99888111, 'aldojasm@gmail.com', 'admin', 'xp111', 'xp111');

INSERT into CATEGORIA (nin_id_categoria, txt_nombre_categoria, txt_descripcion_categoria) VALUES (1, 'Tecnologia', 'Tarjeta de video');

INSERT into PRESENTACION (nin_id_presentacion, txt_nombre_presentacion, txt_descripcion_presentacion) VALUES (1, 'Tecnologia', 'Tarjeta de video');

